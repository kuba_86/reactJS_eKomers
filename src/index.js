import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import configureStore, { history } from './configureStore';

import Home from './components/containers/Home';
import Category from './components/containers/Category';
import Product from './components/containers/Product';
import Cart from './components/containers/Cart';
import Login from './components/containers/Login';
import Register from './components/containers/Register';
import NotFound from './components/screens/NotFound';

import './assets/dist/styles.css';

const App = () => (
  <Provider store={configureStore()}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/category/:slug" component={Category} />
        <Route path="/product/:slug" component={Product} />
        <Route path="/cart" component={Cart} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route component={NotFound} />
      </Switch>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
