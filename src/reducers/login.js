import { LOGIN_UPDATE_FIELD } from '../config/actionTypes';

const initialState = {
  email: '',
  password: ''
};

const login = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_UPDATE_FIELD:
      return {
        ...state,
        [action.name]: action.value
      };
    default:
      return state;
  }
};

export default login;
