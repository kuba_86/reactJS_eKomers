import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import home from './home';
import category from './category';
import product from './product';
import cart from './cart';
import alert from './alert';
import login from './login';
import register from './register';

export default combineReducers({
  router,
  home,
  category,
  product,
  cart,
  login,
  register,
  alert
});
