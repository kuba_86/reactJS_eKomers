import { v4 } from 'uuid';
import {
  CART_ADD_PRODUCT,
  CART_UPDATE_QTY,
  CART_REMOVE_PRODUCT,
  CART_UPDATE_FIELD
} from '../config/actionTypes';

const initialState = {
  products: [],
  address: '',
  postCode: '',
  phone: ''
};

const cart = (state = initialState, action) => {
  switch (action.type) {
    case CART_ADD_PRODUCT:
      return {
        ...state,
        products: [...state.products, { ...action.data, id: `${action.data.id}.${v4()}` }]
      };
    case CART_UPDATE_QTY:
      return {
        ...state,
        products: [
          ...state.products.map((item) => {
            if (item.id === action.id) {
              return {
                ...item,
                number: action.number
              };
            }

            return item;
          })
        ]
      };
    case CART_REMOVE_PRODUCT:
      return {
        ...state,
        products: [...state.products.filter(item => item.id !== action.id)]
      };
    case CART_UPDATE_FIELD:
      return {
        ...state,
        [action.name]: action.value
      };
    default:
      return state;
  }
};

export default cart;
