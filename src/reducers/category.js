import { CATEGORY_FETCH_PRODUCTS, CATEGORY_FETCH_DATA } from '../config/actionTypes';

const initialState = {
  products: [],
  details: {}
};

const category = (state = initialState, action) => {
  switch (action.type) {
    case CATEGORY_FETCH_PRODUCTS:
      return {
        ...state,
        products: action.data
      };
    case CATEGORY_FETCH_DATA:
      return {
        ...state,
        details: action.data
      };
    default:
      return state;
  }
};

export default category;
