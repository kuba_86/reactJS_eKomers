import { REGISTER_UPDATE_FIELD } from '../config/actionTypes';

const initialState = {
  email: '',
  password: '',
  repassword: ''
};

const register = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_UPDATE_FIELD:
      return {
        ...state,
        [action.name]: action.value
      };
    default:
      return state;
  }
};

export default register;
