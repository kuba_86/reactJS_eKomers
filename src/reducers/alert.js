import { v4 } from 'uuid';
import { ALERT_CLOSE, ALERT_ADD, ALERT_CLEAR_ALL } from '../config/actionTypes';

const initialState = {
  list: []
};

const alert = (state = initialState, action) => {
  switch (action.type) {
    case ALERT_ADD:
      return {
        ...state,
        list: [...state.list, { ...action.data, id: v4() }]
      };
    case ALERT_CLOSE:
      return {
        ...state,
        list: [...state.list.filter(item => action.alertId !== item.id)]
      };
    case ALERT_CLEAR_ALL:
      return {
        list: []
      };
    default:
      return state;
  }
};

export default alert;
