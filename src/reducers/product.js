import { PRODUCT_FETCH_DATA, PRODUCT_UPDATE_FIELD } from '../config/actionTypes';

const initialState = {
  products: [],
  details: {}
};

const product = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCT_FETCH_DATA:
      return {
        ...state,
        details: action.data
      };
    case PRODUCT_UPDATE_FIELD:
      return {
        ...state,
        [action.name]: action.value
      };
    default:
      return state;
  }
};

export default product;
