import { HOME_FETCH_PRODUCTS, HOME_FETCH_CATEGORIES } from '../config/actionTypes';

const initialState = {
  categories: [],
  products: []
};

const home = (state = initialState, action) => {
  switch (action.type) {
    case HOME_FETCH_PRODUCTS:
      return {
        ...state,
        products: action.data
      };
    case HOME_FETCH_CATEGORIES:
      return {
        ...state,
        categories: action.data
      };
    default:
      return state;
  }
};

export default home;
