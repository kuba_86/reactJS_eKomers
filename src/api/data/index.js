export default {
  categories: [
    {
      id: 1,
      name: 'Koszulki',
      slug: 'koszulki'
    },
    {
      id: 2,
      name: 'Spodnie',
      slug: 'spodnie'
    },
    {
      id: 3,
      name: 'Buty',
      slug: 'buty'
    },
    {
      id: 4,
      name: 'Kurtki',
      slug: 'kurtki'
    }
  ],
  products: [
    {
      id: 1,
      name: 'Street Symbol Black',
      slug: 'street-symbol-black',
      description:
        'Koszulka marki Outsidewear Materiał: 100% bawełna Michel [73 kg 180cm] ma na zdjęciu rozmiar XL [A6]',
      category: [1],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/full_29930.571010.outsidewear-street_symbol_black.JPG',
      price: '89'
    },
    {
      id: 2,
      name: 'Run The Jewels Goldchain Tee Black',
      slug: 'run-the-jewels-goldchain-tee-black',
      description:
        "Kultowy znak rozpoznawczy rapowego duetu Killer Mike i El-P, tworzących Run The Jewels. Wielokrotnie inspirował świat streetartu kreując oryginalne projekty. Logotyp został również wykorzystany do stworzenia dwóch okładek Marvela, komiksów 'Howard The Duck' oraz 'Deadpool'.",
      category: [1],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/full_81610.1046101.mister-tee-mt573-ladies-rose-tee-black.JPG',
      price: '79'
    },
    {
      id: 3,
      name: 'Raglan Easy Tee TNF White TNF Black',
      slug: 'raglan-easy-tee-tnf-white-tnf-black',
      description:
        'wysokiej jakości, duży nadruk w formie logotypu z przodu, mniejszy nadruk umieszczony na plecach, ozdobna, żakardowa metka wszyta z tyłu, kontrastowe rękawki, Model Kuba [76kg, 181cm] ma na zdjęciu rozmiar M',
      category: [1],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/full_77515.1026048.the-north-face-raglan-easy-tee-tnf-white-tnf-black.JPG',
      price: '119'
    },
    {
      id: 4,
      name: 'Stamp Graphite',
      slug: 'stamp-graphite',
      description:
        'Koszulka marki Outsidewear Materiał: 100% bawełna Michel [73kg, 180cm] ma na zdjęciu rozmiar XL [A1]',
      category: [1],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/t_40884.799899.outsidewear-stamp_charcoal.JPG',
      price: '89'
    },
    {
      id: 5,
      name: 'Q Jogger Woodland Camo',
      slug: 'sq-jogger-woodland-camo',
      description:
        'Trzy kieszenie, Wygodna guma w pasie ze sznurkiem, Nogawki zakończone ściągaczem, Wyprodukowane w Polsce, Model Stanferd [85kg, 185cm] ma na zdjęciu rozmiar L',
      category: [2],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/t_69703.991544.equalizer-eq-jogger-woodland-camo.JPG',
      price: '99'
    },
    {
      id: 6,
      name: 'Klondike Pant Maitland Cotton Black Stone Washed',
      slug: 'klondike-pant-maitland-cotton-black-stone-washed',
      description: 'Spodnie jeansowe marki Carhartt.',
      category: [2],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/t_81596.1046257.carhartt-klondike-pant-maitland-cotton-black-stone-washed.JPG',
      price: '268'
    },
    {
      id: 7,
      name: 'SB Shield Jacket Coaches Obsidian Black',
      slug: 'sb-shield-jacket-coaches-obsidian-black',
      description:
        'Przewiewna, wodoodporna kurtka męska Nike SB Shield Coaches zapewnia suchość i wygodę podczas niesprzyjającej pogody.',
      category: [4],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/full_81369.1045376.nike-sb-shield-jacket-coaches-obsidian-black.JPG',
      price: '259'
    },
    {
      id: 8,
      name: 'MA-1 VF 59 Black',
      slug: '59-black',
      description:
        'Kurtka marki Alpha Industries Wygodna kurtka marki Alpha Industries z kolekcji Flight Jackets. ',
      category: [4],
      image:
        'https://imgurbancity.pl/files/sc_staging_images/product/full_64463.968173.alpha-industries-ma-1-vf-59-black.JPG',
      price: '589'
    }
  ],
  users: [
    {
      id: 1,
      username: 'john',
      email: 'john@dev.dev',
      password: 'johnny'
    },
    {
      id: 2,
      username: 'admin',
      email: 'admin@dev.dev',
      password: 'admin1'
    },
    {
      id: 3,
      username: 'mietek',
      email: 'mietek@budowlaniec.dev',
      password: 'mieciumiszcz'
    }
  ]
};
