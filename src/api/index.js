import _ from 'lodash';
import data from './data';

export const getCategories = () => {
  const categories = data.categories.map(item => ({
    ...item,
    noOfProducts: data.products.filter(product => product.category.includes(item.id)).length || 0
  }));

  return categories;
};

export const getCategory = (field, value) => _.find(data.categories, { [field]: value });

export const getProductsFromCategory = id =>
  _.filter(data.products, product => product.category.includes(id));

export const getProducts = () => data.products;

export const getProduct = (field, value) => _.find(data.products, { [field]: value });

export const getUsers = () => data.users;

export const getUser = (field, value) => _.find(data.users, { [field]: value });

export const userLogin = (email, password) => _.find(data.users, { email, password });
