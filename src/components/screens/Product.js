import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import Layout from '../partials/Layout';
import Sidebar from '../partials/Sidebar';
import Row from '../common/Row';
import Col from '../common/Col';
import InputGroup from '../common/InputGroup';
import Button from '../common/Button';

const Product = ({
  product, categories, onChange, numberToAdd, addToCart
}) => (
  <Layout sidebar>
    <Helmet>
      <title>{`${product.name}`} :: e-komers</title>
    </Helmet>
    <Row>
      <Col md="3">
        <Sidebar categories={categories} />
      </Col>
      <Col md="9" className="product-page">
        <Row>
          <Col md="6">
            <img src={product.image} alt={product.name} />
          </Col>
          <Col md="6">
            <h3>{product.name}</h3>
            <div>{product.description}</div>

            <div className="price">
              {product.price} <span>PLN</span>
            </div>

            <form onSubmit={e => addToCart(e)}>
              <InputGroup
                name="numberToAdd"
                type="number"
                min="1"
                max="100"
                value={numberToAdd}
                onChange={e => onChange(e)}
              >
                Ilość
              </InputGroup>
              <Button>Dodaj do koszyka</Button>
            </form>
          </Col>
        </Row>
      </Col>
    </Row>
  </Layout>
);

Product.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.object),
  // product: PropTypes.objectOf().isRequired,
  onChange: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  numberToAdd: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

Product.defaultProps = {
  categories: [],
  numberToAdd: 0
};

export default Product;
