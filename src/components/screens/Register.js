import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import InputGroup from '../common/InputGroup';
import Button from '../common/Button';
import Row from '../common/Row';
import Col from '../common/Col';
import Layout from '../partials/Layout';

const Register = ({
  email, password, repassword, isLoading, onChange, onSubmit
}) => (
  <Layout>
    <Helmet>
      <title>Rejestracja :: e-komers</title>
    </Helmet>
    <Row className="row--center">
      <Col md="4">
        <div className="headline">
          <span>Rejestracja</span>
        </div>

        <form method="post" onSubmit={e => onSubmit(e)}>
          <InputGroup name="email" type="text" value={email} onChange={e => onChange(e)}>
            Adres email
          </InputGroup>
          <InputGroup name="password" type="password" value={password} onChange={e => onChange(e)}>
            Hasło
          </InputGroup>
          <InputGroup
            name="repassword"
            type="password"
            value={repassword}
            onChange={e => onChange(e)}
          >
            Powtórz hasło
          </InputGroup>

          <Button isLoading={isLoading}>Zarejestruj się</Button>
        </form>
      </Col>
    </Row>
  </Layout>
);

Register.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  repassword: PropTypes.string,
  isLoading: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

Register.defaultProps = {
  email: '',
  password: '',
  repassword: '',
  isLoading: false
};

export default Register;
