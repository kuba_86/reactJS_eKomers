import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import Layout from '../partials/Layout';
import Row from '../common/Row';
import Col from '../common/Col';
import Alert from '../common/Alert';
import Button from '../common/Button';
import InputGroup from '../common/InputGroup';
import Table from '../common/Table';
import TableHead from '../common/TableHead';
import TableBody from '../common/TableBody';
import TableRow from '../common/TableRow';
import TableLabel from '../common/TableLabel';
import TableCol from '../common/TableCol';

const Cart = ({
  products,
  updateQty,
  removeProduct,
  order,
  updateUserData,
  address,
  postCode,
  phone,
  total
}) => (
  <Layout>
    <Helmet>
      <title>Koszyk :: e-komers</title>
    </Helmet>

    <Row className="cart">
      <Col md={products[0] ? '8' : '12'}>
        <div className="headline">
          <span>Koszyk</span>
        </div>

        {products[0] ? (
          <div>
            <Table className="cart__list">
              <TableHead>
                <TableRow>
                  <TableLabel>Nazwa</TableLabel>
                  <TableLabel>Ilość</TableLabel>
                  <TableLabel>Cena</TableLabel>
                  <TableLabel>Akcje</TableLabel>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map(item => (
                  <TableRow key={item.id}>
                    <TableCol>
                      <Link to={`/product/${item.slug}`}>{item.name}</Link>
                    </TableCol>
                    <TableCol>
                      <input
                        name="number"
                        type="number"
                        min="1"
                        value={item.number}
                        onChange={e => updateQty(item.id, e.target.value)}
                      />
                    </TableCol>
                    <TableCol>{item.price} PLN</TableCol>
                    <TableCol className="text-center">
                      <Button type="error" onClick={() => removeProduct(item.id)}>
                        Usuń
                      </Button>
                    </TableCol>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <div className="text-right">
              Całkowity koszt: <b>{total}</b> PLN
            </div>
          </div>
        ) : (
          <Alert type="warning">
            Twój koszyk jest pusty, wróć do <Link to="/">produktów</Link>
          </Alert>
        )}
      </Col>

      {products[0] ? (
        <Col md="4">
          <div className="headline">
            <span>Zamówienie</span>
          </div>

          <form onSubmit={e => order(e)} className="cart__summary">
            <InputGroup
              name="address"
              type="text"
              value={address}
              onChange={e => updateUserData(e)}
            >
              Adres
            </InputGroup>
            <InputGroup
              name="postCode"
              type="text"
              value={postCode}
              onChange={e => updateUserData(e)}
            >
              Kod pocztowy
            </InputGroup>
            <InputGroup name="phone" type="text" value={phone} onChange={e => updateUserData(e)}>
              Numer telefonu
            </InputGroup>

            <Button>Przejdź do płatności</Button>
          </form>
        </Col>
      ) : null}
    </Row>
  </Layout>
);

Cart.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object),
  updateQty: PropTypes.func.isRequired,
  removeProduct: PropTypes.func.isRequired,
  order: PropTypes.func.isRequired,
  updateUserData: PropTypes.func.isRequired,
  address: PropTypes.string,
  postCode: PropTypes.string,
  phone: PropTypes.string,
  total: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

Cart.defaultProps = {
  products: [],
  address: '',
  postCode: '',
  phone: '',
  total: 0
};

export default Cart;
