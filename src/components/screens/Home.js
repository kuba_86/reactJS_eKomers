import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import Layout from '../partials/Layout';
import Sidebar from '../partials/Sidebar';
import Row from '../common/Row';
import Col from '../common/Col';
import Alert from '../common/Alert';
import Product from '../partials/Product';

const Home = ({ categories, products, category }) => (
  <Layout sidebar>
    <Helmet>
      <title>{`${category || 'Strona główna'}`} :: e-komers</title>
    </Helmet>
    <Row>
      <Col md="3">
        <Sidebar categories={categories} />
      </Col>
      <Col md="9">
        {products[0] ? (
          <Row>
            {products.map(item => (
              <Col md="4" key={item.id}>
                <Product
                  name={item.name}
                  description={item.description}
                  slug={item.slug}
                  image={item.image}
                />
              </Col>
            ))}
          </Row>
        ) : (
          <Alert type="warning">
            {category ? `Brak produktów w kategorii ${category}` : 'Brak produktów'}
          </Alert>
        )}
      </Col>
    </Row>
  </Layout>
);

Home.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.object),
  products: PropTypes.arrayOf(PropTypes.object),
  category: PropTypes.string
};

Home.defaultProps = {
  categories: [],
  products: [],
  category: ''
};

export default Home;
