import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import Layout from '../partials/Layout';

const NotFound = () => (
  <Layout>
    <Helmet>
      <title>Nie znaleziono :: e-komers</title>
    </Helmet>
    <div className="notFound">
      <div className="notFound__content">
        <h3>Strona nie została znaleziona :(</h3>
        <h6>
          Przejdź do <Link to="/">strony głównej</Link>
        </h6>
      </div>
    </div>
  </Layout>
);

export default NotFound;
