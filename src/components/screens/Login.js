import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import InputGroup from '../common/InputGroup';
import Button from '../common/Button';
import Row from '../common/Row';
import Col from '../common/Col';
import Layout from '../partials/Layout';

const Login = ({
  email, password, isLoading, onChange, onSubmit
}) => (
  <Layout>
    <Helmet>
      <title>Logowanie :: e-komers</title>
    </Helmet>
    <Row className="row--center">
      <Col md="4">
        <div className="headline">
          <span>Logowanie</span>
        </div>

        <form method="post" onSubmit={e => onSubmit(e)}>
          <InputGroup name="email" type="text" value={email} onChange={e => onChange(e)}>
            Adres email
          </InputGroup>
          <InputGroup name="password" type="password" value={password} onChange={e => onChange(e)}>
            Hasło
          </InputGroup>

          <Button isLoading={isLoading}>Zaloguj się</Button>
        </form>
      </Col>
    </Row>
  </Layout>
);

Login.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  isLoading: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

Login.defaultProps = {
  email: '',
  password: '',
  isLoading: false
};

export default Login;
