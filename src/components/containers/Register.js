import { connect } from 'react-redux';
import Register from '../screens/Register';
import { onChange } from '../../actions/registerActions';
import { addAlert } from '../../actions/alertActions';

const mapStateToProps = state => ({
  email: state.register.email,
  password: state.register.password,
  repassword: state.register.repassword
});

const mapDispatchToProps = dispatch => ({
  onChange: (e) => {
    dispatch(onChange(e.target.name, e.target.value));
  },
  onSubmit: (e) => {
    e.preventDefault();

    dispatch(addAlert({
      type: 'warning',
      text: 'Rejestracja do serwisu jest zablokowana, skontaktuj się z administratorem'
    }));

    console.log('Not implemented :p');
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
