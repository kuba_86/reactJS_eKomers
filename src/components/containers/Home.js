import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Home from '../screens/Home';
import { fetchProducts, fetchCategories } from '../../actions/homeActions';

class HomeContainer extends Component {
  componentDidMount() {
    this.props.fetchProducts();
    this.props.fetchCategories();
  }

  render() {
    return <Home {...this.props} />;
  }
}

HomeContainer.propTypes = {
  fetchProducts: PropTypes.func.isRequired,
  fetchCategories: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  products: state.home.products,
  categories: state.home.categories
});

const mapDispatchToProps = dispatch => ({
  fetchProducts: () => {
    dispatch(fetchProducts());
  },
  fetchCategories: () => {
    dispatch(fetchCategories());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
