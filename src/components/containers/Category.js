import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Home from '../screens/Home';
import { fetchCategories } from '../../actions/homeActions';
import { fetchCategory } from '../../actions/categoryActions';

class CategoryContainer extends Component {
  componentDidMount() {
    return this.fetchData();
  }

  componentDidUpdate(prevProps) {
    if (!prevProps) {
      return false;
    }

    if (this.props.slug === this.props.match.params.slug) {
      return false;
    }

    return this.fetchData();
  }

  fetchData() {
    this.props.fetchCategories();
    this.props.fetchCategory();
  }

  render() {
    return <Home category={this.props.name} {...this.props} />;
  }
}

CategoryContainer.propTypes = {
  fetchCategories: PropTypes.func.isRequired,
  fetchCategory: PropTypes.func.isRequired,
  slug: PropTypes.string,
  name: PropTypes.string,
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string.isRequired
    })
  }).isRequired
};

CategoryContainer.defaultProps = {
  slug: '',
  name: ''
};

const mapStateToProps = state => ({
  products: state.category.products,
  categories: state.home.categories,
  slug: state.category.details.slug,
  name: state.category.details.name
});

const mapDispatchToProps = (dispatch, props) => ({
  fetchCategories: () => {
    dispatch(fetchCategories());
  },
  fetchCategory: () => {
    dispatch(fetchCategory(props.match.params.slug));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryContainer);
