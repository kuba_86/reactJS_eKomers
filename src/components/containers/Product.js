import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Product from '../screens/Product';
import { fetchCategories } from '../../actions/homeActions';
import { fetchProduct, onChange } from '../../actions/productActions';
import { onAddToCart } from '../../actions/cartActions';

class ProductContainer extends Component {
  constructor(props) {
    super(props);

    this.addToCart = this.addToCart.bind(this);
  }

  componentDidMount() {
    this.props.fetchProduct();
    this.props.fetchCategories();
  }

  addToCart(e) {
    e.preventDefault();

    const { numberToAdd, product } = this.props;

    this.props.addToCartSubmit(numberToAdd, product);
  }

  render() {
    return <Product addToCart={this.addToCart} {...this.props} />;
  }
}

ProductContainer.propTypes = {
  fetchCategories: PropTypes.func.isRequired,
  fetchProduct: PropTypes.func.isRequired,
  addToCartSubmit: PropTypes.func.isRequired,
  numberToAdd: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  // product: PropTypes.objectOf().isRequired
};

ProductContainer.defaultProps = {
  numberToAdd: 1
};

const mapStateToProps = state => ({
  product: state.product.details,
  categories: state.home.categories,
  numberToAdd: state.product.numberToAdd
});

const mapDispatchToProps = (dispatch, props) => ({
  fetchCategories: () => {
    dispatch(fetchCategories());
  },
  fetchProduct: () => {
    dispatch(fetchProduct(props.match.params.slug));
  },
  onChange: (e) => {
    dispatch(onChange(e.target.name, e.target.value));
  },
  addToCartSubmit: (numberToAdd, product) => {
    dispatch(onAddToCart(numberToAdd, product));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer);
