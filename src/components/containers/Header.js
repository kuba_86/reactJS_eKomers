import { connect } from 'react-redux';
import { closeAlert } from '../../actions/alertActions';
import Header from '../partials/Header';

const mapStateToProps = state => ({
  productsInCart: state.cart.products.length || 0
});

const mapDispatchToProps = dispatch => ({
  onClose: (alertId) => {
    dispatch(closeAlert(alertId));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
