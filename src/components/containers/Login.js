import { connect } from 'react-redux';
import Login from '../screens/Login';
import { onChange } from '../../actions/loginActions';
import { addAlert } from '../../actions/alertActions';

const mapStateToProps = state => ({
  email: state.login.email,
  password: state.login.password
});

const mapDispatchToProps = dispatch => ({
  onChange: (e) => {
    dispatch(onChange(e.target.name, e.target.value));
  },
  onSubmit: (e) => {
    e.preventDefault();

    dispatch(addAlert({
      type: 'warning',
      text: 'Logowanie do serwisu jest zablokowane, skontaktuj się z administratorem'
    }));

    console.log('Not implemented :p');
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
