import { connect } from 'react-redux';
import _ from 'lodash';
import { addAlert } from '../../actions/alertActions';
import { removeProduct, updateQty, onChange } from '../../actions/cartActions';
import Cart from '../screens/Cart';

const mapStateToProps = state => ({
  products: state.cart.products,
  address: state.cart.address,
  postCode: state.cart.postCode,
  phone: state.cart.phone,
  total: _.reduce(
    state.cart.products,
    (sum, item) => sum + Number(item.price) * Number(item.number),
    0
  )
});

const mapDispatchToProps = dispatch => ({
  updateQty: (id, number) => {
    dispatch(updateQty(id, number));
  },
  removeProduct: (id) => {
    dispatch(removeProduct(id));
  },
  updateUserData: (e) => {
    dispatch(onChange(e.target.name, e.target.value));
  },
  order: (e) => {
    e.preventDefault();

    dispatch(addAlert({
      type: 'warning',
      text: 'Aby złożyć zamówienie musisz być zalogowany'
    }));

    console.log('Not implemented :p');
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
