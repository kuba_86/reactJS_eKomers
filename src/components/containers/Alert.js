import { connect } from 'react-redux';
import { closeAlert } from '../../actions/alertActions';
import Alert from '../partials/Alert';

const mapStateToProps = state => ({
  list: state.alert.list
});

const mapDispatchToProps = dispatch => ({
  onClose: (alertId) => {
    dispatch(closeAlert(alertId));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Alert);
