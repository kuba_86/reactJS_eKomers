import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Product = ({
  image, slug, name, description, ...rest
}) => (
  <div className="product" {...rest}>
    <div className="product__image">
      <Link to={`/product/${slug}`}>
        <img src={image} alt={name} />
      </Link>
    </div>
    <div className="product__content">
      <h3>
        <Link to={`/product/${slug}`}>{name}</Link>
      </h3>
      <p>{description.substr(0, 50)}...</p>
      <Link className="btn btn--primary" to={`/product/${slug}`}>
        Zobacz produkt
      </Link>
    </div>
  </div>
);

Product.propTypes = {
  image: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

export default Product;
