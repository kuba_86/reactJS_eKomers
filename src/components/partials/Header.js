import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Wrapper from '../common/Wrapper';
import Row from '../common/Row';
import Col from '../common/Col';
import Badge from '../common/Badge';

const Header = ({ productsInCart }) => (
  <div className="header">
    <Wrapper>
      <Row>
        <Col md="6">
          <h3 className="logo">
            <Link to="/">e-komers</Link>
          </h3>
          <ul className="nav">
            <li>
              <NavLink exact to="/">
                Główna
              </NavLink>
            </li>
            <li>
              <NavLink to="/cart">
                Koszyk <Badge value={productsInCart} />
              </NavLink>
            </li>
          </ul>
        </Col>
        <Col md="6">
          <div className="text-right">
            <ul className="nav">
              <li>
                <NavLink to="/login">Logowanie</NavLink>
              </li>
              <li>
                <NavLink to="/register">Rejestracja</NavLink>
              </li>
            </ul>
          </div>
        </Col>
      </Row>
    </Wrapper>
  </div>
);

Header.propTypes = {
  productsInCart: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};

export default Header;
