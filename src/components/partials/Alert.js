import React from 'react';
import PropTypes from 'prop-types';
import AlertComponent from '../common/Alert';
import Col from '../common/Col';

const Alert = ({ list, onClose }) =>
  (list[0] ? (
    <Col md="12" className="alertsContainer">
      {list.map(item => (
        <AlertComponent
          key={item.id}
          id={item.id}
          type={item.type}
          onClose={() => onClose(item.id)}
        >
          {item.text}
        </AlertComponent>
      ))}
    </Col>
  ) : null);

Alert.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  onClose: PropTypes.func
};

Alert.defaultProps = {
  onClose: null
};

export default Alert;
