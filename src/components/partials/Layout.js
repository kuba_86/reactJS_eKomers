import React from 'react';
import PropTypes from 'prop-types';
import AlertContainer from '../containers/Alert';
import HeaderContainer from '../containers/Header';
import Footer from './Footer';
import Wrapper from '../common/Wrapper';
import Row from '../common/Row';
import Col from '../common/Col';

const Layout = ({ children }) => (
  <Wrapper>
    <HeaderContainer />

    <Row>
      <div className="layout">
        <AlertContainer />
        <Col md="12">{children}</Col>
      </div>
    </Row>

    <Footer />
  </Wrapper>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
