import React from 'react';
import Wrapper from '../common/Wrapper';
import Row from '../common/Row';
import Col from '../common/Col';

const Footer = () => (
  <div className="footer">
    <Wrapper>
      <Row>
        <Col md="12">&copy; 2018, e-komers</Col>
      </Row>
    </Wrapper>
  </div>
);

export default Footer;
