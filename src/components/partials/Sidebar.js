import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Badge from '../common/Badge';

const Sidebar = ({ categories, ...rest }) => (
  <div className="sidebar" {...rest}>
    <h3 className="sidebar__header">
      <span>Kategorie</span>
    </h3>
    <ul className="sidebar__menu">
      {categories.map(item => (
        <li key={item.id}>
          <NavLink to={`/category/${item.slug}`}>
            {item.name}
            <Badge value={item.noOfProducts} className="sidebar__badge" />
          </NavLink>
        </li>
      ))}
    </ul>
  </div>
);

Sidebar.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.object)
};

Sidebar.defaultProps = {
  categories: []
};

export default Sidebar;
