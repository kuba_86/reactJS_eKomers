import React from 'react';
import PropTypes from 'prop-types';

const TableLabel = ({ className, children, ...rest }) => (
  <th className={`table__label ${className}`} {...rest}>
    {children}
  </th>
);

TableLabel.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

TableLabel.defaultProps = {
  className: ''
};

export default TableLabel;
