import React from 'react';
import PropTypes from 'prop-types';

const Alert = ({
  type, className, onClose, children, ...rest
}) => {
  let closeButton;

  if (onClose) {
    closeButton = (
      <button className="alert__close" onClick={onClose}>
        &times;
      </button>
    );
  }

  return (
    <div className={`alert alert--${type} ${className}`} {...rest}>
      {closeButton}
      {children}
    </div>
  );
};

Alert.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.string.isRequired,
  onClose: PropTypes.func,
  className: PropTypes.string
};

Alert.defaultProps = {
  onClose: null,
  className: ''
};

export default Alert;
