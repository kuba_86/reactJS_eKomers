import React from 'react';
import PropTypes from 'prop-types';

const Table = ({ className, children, ...rest }) => (
  <table className={`table ${className}`} {...rest}>
    {children}
  </table>
);

Table.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

Table.defaultProps = {
  className: ''
};

export default Table;
