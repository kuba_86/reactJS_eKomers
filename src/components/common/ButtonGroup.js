import React from 'react';
import PropTypes from 'prop-types';

const ButtonGroup = ({ children, className, ...rest }) => (
  <div className={`btn-group ${className}`} {...rest}>
    {children}
  </div>
);

ButtonGroup.propTypes = {
  children: PropTypes.string.isRequired,
  className: PropTypes.string
};

ButtonGroup.defaultProps = {
  className: ''
};

export default ButtonGroup;
