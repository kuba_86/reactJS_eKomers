import React from 'react';
import PropTypes from 'prop-types';

const Col = ({
  sm, md, lg, className, children, ...rest
}) => {
  const classes = [];

  if (sm) {
    classes.push(`col--sm${sm}`);
  }

  if (md) {
    classes.push(`col--md${md}`);
  }

  if (lg) {
    classes.push(`col--lg${md}`);
  }

  return (
    <div className={`col ${classes.join(' ')} ${className}`} {...rest}>
      {children}
    </div>
  );
};

Col.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  sm: PropTypes.string,
  md: PropTypes.string,
  lg: PropTypes.string
};

Col.defaultProps = {
  className: '',
  sm: null,
  md: null,
  lg: null
};

export default Col;
