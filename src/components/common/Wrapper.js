import React from 'react';
import PropTypes from 'prop-types';

const Wrapper = ({ className, children, ...rest }) => (
  <div className={`wrapper ${className}`} {...rest}>
    {children}
  </div>
);

Wrapper.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

Wrapper.defaultProps = {
  className: ''
};

export default Wrapper;
