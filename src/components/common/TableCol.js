import React from 'react';
import PropTypes from 'prop-types';

const TableCol = ({ className, children, ...rest }) => (
  <td className={`table__col ${className}`} {...rest}>
    {children}
  </td>
);

TableCol.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

TableCol.defaultProps = {
  className: ''
};

export default TableCol;
