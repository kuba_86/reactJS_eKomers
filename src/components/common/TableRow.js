import React from 'react';
import PropTypes from 'prop-types';

const TableRow = ({ className, children, ...rest }) => (
  <tr className={`table__row ${className}`} {...rest}>
    {children}
  </tr>
);

TableRow.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

TableRow.defaultProps = {
  className: ''
};

export default TableRow;
