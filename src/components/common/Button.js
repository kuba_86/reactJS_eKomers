import React from 'react';
import PropTypes from 'prop-types';
import Spinner from './Spinner';

const Button = ({
  children, type, isLoading, className, ...rest
}) => (
  <button className={`btn btn--${type} ${className}`} disabled={isLoading ? true : null} {...rest}>
    {isLoading ? (
      <span>
        Ładowanie <Spinner size="small" className="display-inline-block" />
      </span>
    ) : (
      children
    )}
  </button>
);

Button.propTypes = {
  children: PropTypes.string.isRequired,
  type: PropTypes.string,
  isLoading: PropTypes.bool,
  className: PropTypes.string
};

Button.defaultProps = {
  type: 'primary',
  isLoading: false,
  className: ''
};

export default Button;
