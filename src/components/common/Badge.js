import React from 'react';
import PropTypes from 'prop-types';

const Badge = ({
  value, type, className, ...rest
}) => (
  <span className={`badge badge--${type} ${className}`} {...rest}>
    {value}
  </span>
);

Badge.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  type: PropTypes.string,
  className: PropTypes.string
};

Badge.defaultProps = {
  type: 'primary',
  className: ''
};

export default Badge;
