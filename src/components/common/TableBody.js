import React from 'react';
import PropTypes from 'prop-types';

const TableBody = ({ className, children, ...rest }) => (
  <tbody className={`table__body ${className}`} {...rest}>
    {children}
  </tbody>
);

TableBody.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

TableBody.defaultProps = {
  className: ''
};

export default TableBody;
