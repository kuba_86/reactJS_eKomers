import React from 'react';
import PropTypes from 'prop-types';

const InputGroup = ({
  name, className, children, ...rest
}) => (
  <label htmlFor={name}>
    {children}
    <input id={name} name={name} {...rest} />
  </label>
);

InputGroup.propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.node,
  className: PropTypes.string
};

InputGroup.defaultProps = {
  children: null,
  className: ''
};

export default InputGroup;
