import React from 'react';
import PropTypes from 'prop-types';

const Spinner = ({
  text, size, className, ...rest
}) => {
  let classes;

  if (size) {
    classes = `spinner--${size}`;
  }

  return (
    <div className={`spinner ${classes} ${className}`} {...rest}>
      <span className="spinner__text">{text}</span>

      <svg className="spinner__dots" viewBox="0 0 40 15" xmlns="http://www.w3.org/2000/svg">
        <circle cx="5" cy="15" r="5" />
        <circle cx="20" cy="15" r="5" />
        <circle cx="35" cy="15" r="5" />
      </svg>
    </div>
  );
};

Spinner.propTypes = {
  text: PropTypes.string,
  size: PropTypes.string,
  className: PropTypes.string
};

Spinner.defaultProps = {
  text: '',
  size: 'medium',
  className: ''
};

export default Spinner;
