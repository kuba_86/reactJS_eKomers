import React from 'react';
import PropTypes from 'prop-types';

const TableHead = ({ className, children, ...rest }) => (
  <thead className={`table__head ${className}`} {...rest}>
    {children}
  </thead>
);

TableHead.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

TableHead.defaultProps = {
  className: ''
};

export default TableHead;
