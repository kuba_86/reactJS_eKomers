const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

gulp.task('img', () => {
  gulp
    .src('./src/img/**')
    .pipe($.imagemin())
    .pipe(gulp.dest('./dist/img'));
});

gulp.task('sass', () => {
  gulp
    .src('./src/**/*.{css,scss}')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      outputStyle: 'compressed'
    }))
    .pipe($.concat('styles.css'))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions']
    }))
    .pipe($.sourcemaps.write('/'))
    .pipe(gulp.dest('dist'));
});

gulp.task('watch', () => {
  gulp.watch('./src/**/*.{css,scss}', ['sass']);
});

gulp.task('default', ['watch', 'sass']);
