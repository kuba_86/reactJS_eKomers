import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';
import reducers from './reducers';

export const history = createHistory();

const logger = createLogger();
const router = routerMiddleware(history);

const configureStore = initialState =>
  createStore(reducers, initialState, applyMiddleware(thunk, logger, router));

export default configureStore;
