import { LOGIN_UPDATE_FIELD } from '../config/actionTypes';

export const onChange = (name, value) => ({
  type: LOGIN_UPDATE_FIELD,
  name,
  value
});
