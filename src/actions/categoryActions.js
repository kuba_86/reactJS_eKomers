import { push } from 'react-router-redux';
import { getCategory, getProductsFromCategory } from '../api/';
import { CATEGORY_FETCH_PRODUCTS, CATEGORY_FETCH_DATA } from '../config/actionTypes';

export const fetchCategory = slug => (dispatch) => {
  const category = getCategory('slug', slug);

  if (category) {
    dispatch({
      type: CATEGORY_FETCH_DATA,
      data: category
    });

    dispatch({
      type: CATEGORY_FETCH_PRODUCTS,
      data: getProductsFromCategory(category.id)
    });
  } else {
    dispatch(push('/not-found'));
  }
};
