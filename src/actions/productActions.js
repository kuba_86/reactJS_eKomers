import { push } from 'react-router-redux';
import { getProduct } from '../api/';
import { PRODUCT_FETCH_DATA, PRODUCT_UPDATE_FIELD } from '../config/actionTypes';

export const fetchProduct = slug => (dispatch) => {
  const product = getProduct('slug', slug);

  if (product) {
    dispatch({
      type: PRODUCT_FETCH_DATA,
      data: product
    });
  } else {
    dispatch(push('/not-found'));
  }
};

export const onChange = (name, value) => ({
  type: PRODUCT_UPDATE_FIELD,
  name,
  value
});
