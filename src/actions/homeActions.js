import { getCategories, getProducts } from '../api/';
import { HOME_FETCH_PRODUCTS, HOME_FETCH_CATEGORIES } from '../config/actionTypes';

export const fetchCategories = () => (dispatch) => {
  dispatch({
    type: HOME_FETCH_CATEGORIES,
    data: getCategories()
  });
};

export const fetchProducts = () => (dispatch) => {
  dispatch({
    type: HOME_FETCH_PRODUCTS,
    data: getProducts()
  });
};
