import { ALERT_CLOSE, ALERT_ADD, ALERT_CLEAR_ALL } from '../config/actionTypes';

export const closeAlert = alertId => ({
  type: ALERT_CLOSE,
  alertId
});

export const clearAlerts = () => ({
  type: ALERT_CLEAR_ALL
});

export const addAlert = data => (dispatch) => {
  dispatch({
    type: ALERT_CLEAR_ALL
  });

  dispatch({
    type: ALERT_ADD,
    data
  });
};
