import {
  CART_ADD_PRODUCT,
  CART_UPDATE_QTY,
  CART_REMOVE_PRODUCT,
  CART_UPDATE_FIELD
} from '../config/actionTypes';
import { addAlert, clearAlerts } from './alertActions';

export const onAddToCart = (number, product) => (dispatch) => {
  dispatch(addAlert({
    type: 'success',
    text: `Produkt "${product.name}" został dodany koszyka!`
  }));

  dispatch({
    type: CART_ADD_PRODUCT,
    data: {
      ...product,
      number
    }
  });
};

export const updateQty = (id, number) => (dispatch) => {
  dispatch(clearAlerts());
  dispatch({
    type: CART_UPDATE_QTY,
    id,
    number
  });
};

export const removeProduct = id => ({
  type: CART_REMOVE_PRODUCT,
  id
});

export const onChange = (name, value) => ({
  type: CART_UPDATE_FIELD,
  name,
  value
});
