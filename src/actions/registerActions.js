import { REGISTER_UPDATE_FIELD } from '../config/actionTypes';

export const onChange = (name, value) => ({
  type: REGISTER_UPDATE_FIELD,
  name,
  value
});
